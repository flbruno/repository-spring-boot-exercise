package com.spring.boot.exercise.service;

import com.spring.boot.exercise.model.MoviesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MoviesServiceImpl implements MoviesService {

  private final RestTemplate restTemplate;
  private static final Integer PAGE_MAX = 2;

  public MoviesServiceImpl(RestTemplateBuilder restTemplate) {
    this.restTemplate = restTemplate.build();
  }

  @Override
  public List<String> getMoviesTitle(String title) {

    List<String> movies = new ArrayList<String>();
    int pageNumber = 0;

    do {
      pageNumber = pageNumber + 1;

      MoviesResponse response =
          restTemplate.getForObject(
              "https://jsonmock.hackerrank.com/api/movies/search/?Title="
                  + title
                  + "&page="
                  + pageNumber,
              MoviesResponse.class);
      movies.addAll(
          response.getMovieTitles().stream()
              .map(dataTitle -> dataTitle.getTitle())
              .collect(Collectors.toList()));

    } while (pageNumber != PAGE_MAX);

    movies.sort(Comparator.naturalOrder());
    movies.forEach(System.out::println);

    return movies;
  }
}
