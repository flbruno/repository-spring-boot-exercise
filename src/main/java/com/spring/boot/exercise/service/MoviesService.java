package com.spring.boot.exercise.service;

import java.util.List;

public interface MoviesService {

  List<String> getMoviesTitle(String title);
}
