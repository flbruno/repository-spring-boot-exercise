package com.spring.boot.exercise.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class MoviesResponse {

  @JsonProperty("page")
  private String page;

  @JsonProperty("per_page")
  private String per_page;

  @JsonProperty("total")
  private String total;

  @JsonProperty("total_pages")
  private String total_pages;

  @JsonProperty("data")
  private List<MoviesDataTitle> movieTitles;
}
