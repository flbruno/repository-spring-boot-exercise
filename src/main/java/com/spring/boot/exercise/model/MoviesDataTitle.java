package com.spring.boot.exercise.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class MoviesDataTitle {

  @JsonProperty("Poster")
  private String poster;

  @JsonProperty("Title")
  private String title;

  @JsonProperty("Type")
  private String type;

  @JsonProperty("Year")
  private String year;

  @JsonProperty("imdbID")
  private String imdbID;
}
