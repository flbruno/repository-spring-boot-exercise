import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@SpringBootTest
class ExerciseSpringBootApplicationTests {

  @ArchTest
  public static final ArchRule javaUtilDateShouldNotBeUsed =
      noClasses().should().accessClassesThat().areAssignableTo(Date.class);

  @ArchTest
  public static final ArchRule javaUtilDateShouldNotBeImported =
      noClasses().should().dependOnClassesThat().areAssignableTo(Date.class);
}
