package com.spring.boot.exercise;

import com.spring.boot.exercise.controller.MoviesController;
import com.spring.boot.exercise.service.MoviesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MoviesController.class)
public class ExerciseGetMoviesByTitleTest {

  private static final String URL = "/movies/search";
  private static final String TITLE = "spiderman";
  @Autowired private MockMvc mockMvc;

  private MockHttpServletRequestBuilder requestBuilder;

  @MockBean private MoviesService moviesService;

  @Before
  public void setup() {
    requestBuilder = get(URL).servletPath(URL).param("Title", TITLE);
  }

  @Test
  public void testGetMovies_GetAllMoviesByTitle_ReturnResponseSuccessfully() throws Exception {

    // Asserts
    mockMvc.perform(requestBuilder).andExpect(status().isOk());
  }
}
